﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Kolokwium_p3_poprawa
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            // Zadanie 1 --------------------------------
            var rand = new Random();
            var mieszkania = new List<Mieszkanie>();

            var mieszkanie = new Mieszkanie();

            for (int i = 0; i < 50; i++)
            {
                mieszkania.Add(new Mieszkanie(rand.Next(30, 50), "Mieszkanie " + rand.Next() + "", rand.Next(100, 1500)));
            }

            var sortedList = mieszkania.OrderBy(m => m.OdlegloscOdcentrum).ThenBy(m => m.Nazwa);

            foreach (var item in sortedList)
            {
                Console.WriteLine(item.Nazwa.ToString());
            }

            // -------------------------------------------
            // Zadanie 2

            var test = new Test(12, "aa", "bb", true, "cc");

            Console.WriteLine(test.Dekonstruktor1());
            Console.WriteLine();
            Console.WriteLine(test.Dekonstruktor2());

            // -------------------------------------------
            // 4

            var zamki = new List<IZamek>();

            zamki.Add(new ZamekPIN(false, 1234));
            zamki.Add(new ZamekElektroniczny(false));

            foreach (var item in zamki)
            {
                item.Otworz();
                item.Zamknij();
                item.Zamknij();
            }

        }
    }
}
