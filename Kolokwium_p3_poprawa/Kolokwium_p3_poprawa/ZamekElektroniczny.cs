﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    class ZamekElektroniczny : IZamek
    {
        bool open = false;

        public ZamekElektroniczny(bool open)
        {
            this.open = open;
        }

        public void Otworz()
        {
            if (open == false)
            {
                Console.WriteLine("Put magnetic card");
                Console.ReadKey();
                Console.WriteLine("[Log]: Electronic lock open at " + DateTime.Now);
                open = true;
            } else
            {
                Console.WriteLine("Cant open opened doors");
                open = false;
            }
        }

        public void Zamknij()
        {
            if (open == true) {
                Console.WriteLine("Put magnetic card");
                Console.ReadKey();
                Console.WriteLine("[Log]: Electronic lock locked at " + DateTime.Now);
                open = false;
                    }
            else
            {
                Console.WriteLine("Doors already locked");
                open = true;
            }
        }
    }
}
