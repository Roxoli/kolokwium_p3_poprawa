﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    class Ochrona
    {

        public event EventHandler Alarm;
        public Ochrona()
        {
            Alarm += AlarmHandler;
        }

        public void AlarmHandler(object obj, EventArgs args)
        {
            Console.WriteLine("Something");
        }

        public void start()
        {
            Alarm.Invoke(this, EventArgs.Empty);
        }

        


    }
}
