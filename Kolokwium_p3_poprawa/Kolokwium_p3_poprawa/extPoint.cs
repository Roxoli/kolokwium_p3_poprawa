﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    // zadanie 3
    static class extPoint
    {
        static void quadrant(this Point point)
        {
            if (point.X > 0 && point.Y > 0)
                Console.WriteLine("lies in First quadrant");

            else if (point.X < 0 && point.Y > 0)
                Console.WriteLine("lies in Second quadrant");

            else if (point.X < 0 && point.Y < 0)
                Console.WriteLine("lies in Third quadrant");

            else if (point.X > 0 && point.Y < 0)
                Console.WriteLine("lies in Fourth quadrant");

            else if (point.X == 0 && point.Y > 0)
                Console.WriteLine("lies at positive y axis");

            else if (point.X == 0 && point.Y < 0)
                Console.WriteLine("lies at negative y axis");

            else if (point.Y == 0 && point.X < 0)
                Console.WriteLine("lies at negative x axis");

            else if (point.Y == 0 && point.X > 0)
                Console.WriteLine("lies at positive x axis");

            else
                Console.WriteLine("lies at origin");
        }
    }
}
