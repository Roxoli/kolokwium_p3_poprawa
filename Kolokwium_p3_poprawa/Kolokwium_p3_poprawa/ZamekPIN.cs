﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    class ZamekPIN : IZamek
    {
        bool open = false;
        int pin = 1234;

        public ZamekPIN(bool open, int pin)
        {
            this.open = open;
            this.pin = pin;
        }

        public void Otworz()
        {
           if(open == false)
            {
                Console.WriteLine("Podaj pin");
                var userPin = Convert.ToInt32(Console.ReadLine());
                    if(userPin == pin)
                {
                    open = true;
                    Console.WriteLine("Correct pin, doors open");
                } else
                {
                    Console.WriteLine("Niepoprawny pin!");
                }
            } else
            {
                Console.WriteLine("Cant open opened doors");
            }
        }

        public void Zamknij()
        {
            if(open == true)
            {
                open = false;
                Console.WriteLine("Podaj pin");
            } else
            {
                Console.WriteLine("Doors already locked");
            }
        }
    }
}
