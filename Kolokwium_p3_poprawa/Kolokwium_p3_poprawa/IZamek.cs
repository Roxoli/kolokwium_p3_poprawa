﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    interface IZamek
    {
        void Otworz();
        void Zamknij();
    }
}
