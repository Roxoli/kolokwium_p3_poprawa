﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    class Test
    {
        public int Id { get; set; }
        public string Nazwa{ get; set; }
        public string Opis { get; set; }
        public bool Powodzenie { get; set; }
        public string Nazwisko { get; set; }

        public Test(int id, string nazwa, string opis, bool powodzenie, string nazwisko)
        {
            Id = id;
            Nazwa = nazwa;
            Opis = opis;
            Powodzenie = powodzenie;
            Nazwisko = nazwisko;
        }

        public (int, bool) Dekonstruktor1()
        {
            return (Id, Powodzenie);
        }

        public (int, string, string, bool, string) Dekonstruktor2()
        {
            return (Id, Nazwa, Opis, Powodzenie, Nazwisko);
        }


    }
}
