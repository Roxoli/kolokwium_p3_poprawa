﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium_p3_poprawa
{
    class Mieszkanie
    {

        public float Metraz { get; set; }
        public string Nazwa { get; set; }
        public float OdlegloscOdcentrum { get; set; }


        public Mieszkanie()
        {
        }
        public Mieszkanie(float metraz, string nazwa, float odlegloscOdcentrum)
        {
            Metraz = metraz;
            Nazwa = nazwa;
            OdlegloscOdcentrum = odlegloscOdcentrum;
        }

     

    }
}
